import s3fs
import json
import boto3
import pandas as pd
from rename_list import rename_list

# reads file from s3 and fills empty cells
# file name: 's3://s3-csv-to-dynamo/test_3_hadoop'
def read_file(file_name):
    column_names = ['user_session_id', 'DISCARDED_1', 'current_view', 'index_id', 'DISCARDED_2', 'user_language', 'host_name', 'result_count', 'request_type', 'query', 'query_id', 'DISCARDED_3', 'clicked_result_title', 'clicked_result_url', 'clicked_solr_id', 'date', 'hour']
    data = pd.read_csv(file_name, sep='\x01|\u0001', encoding='raw_unicode_escape', names=column_names, engine='python')
    
    data.fillna("000", inplace=True)
    all_columns = list(data) # Creates list of all column headers
    data[all_columns] = data[all_columns].astype(str) # convert all data to string

    return data


# takes input a data frame & groups all columns by index_id and date
def group_data(data):   
    data['timestamp'] = data['date'] + ' ' + data['hour']

    grouped_data_list = []
    grouped_data = data.groupby(['index_id','timestamp'])

    for group in grouped_data:
        group_dict = {}

        group_dict['index_id'] = group[0][0]
        group_dict['timestamp'] = group[0][1]
        # group_dict['date'] = group[1]['date'].to_list()
        group_dict['query'] = group[1]['query'].to_list()
        group_dict['host_name'] = group[1]['host_name'].to_list()
        # group_dict['current_view'] = group[1]['current_view'].to_list()
        group_dict['user_language'] = group[1]['user_language'].to_list()
        group_dict['query_id'] = group[1]['query_id'].to_list()
        group_dict['result_count'] = group[1]['result_count'].to_list()
        group_dict['request_type'] = group[1]['request_type'].to_list()
        group_dict['clicked_result_title'] = group[1]['clicked_result_title'].to_list()
        group_dict['clicked_result_url'] = group[1]['clicked_result_url'].to_list()
        group_dict['clicked_solr_id'] = group[1]['clicked_solr_id'].to_list()
        # group_dict['user_session_id'] = group[1]['user_session_id'].to_list()
        # group_dict['hour'] = group[1]['hour'].to_list()

        if group_dict['index_id'] in rename_list: group_dict['index_id'] = rename_list[group_dict['index_id']] # corrects the index for some accounts
        if ":" in group_dict['index_id']: group_dict['index_id'] = group_dict['index_id'].split(":")[1] # parses the ("something:number" -> "number")
        print(group_dict['index_id'])
        print(group_dict['timestamp'])

        grouped_data_list.append(group_dict)
    
    return grouped_data_list


# adds grouped data to database
def add_record_to_db(dynamo_table, grouped_data_list):
    good_counter = 0
    
    with dynamo_table.batch_writer() as batch:
        for item in grouped_data_list:
            name = 'GOOD_' + str(good_counter)
            with open(name, 'a') as the_file:
                the_file.write(json.dumps(item))

            if item["index_id"] != "" and item["timestamp"] != "":

                batch.put_item(
                    Item = {
                        "index_id": item["index_id"],
                        "timestamp": item["timestamp"],
                        "query": item["query"],
                        "host_name": item["host_name"],
                        #"current_view": item["current_view"],
                        "user_language": item["user_language"],
                        "query_id": item["query_id"],
                        "result_count": item["result_count"],
                        "request_type": item["request_type"],
                        "clicked_result_title": item["clicked_result_title"],
                        "clicked_result_url": item["clicked_result_url"],
                        "clicked_solr_id": item["clicked_solr_id"],
                        # "user_session_id": item["user_session_id"],
                        # "hour": item["hour"]
                        }
                    )
                good_counter+=1
                print(str(good_counter) + " rows added to dynamo db :-)" )
    
    return True


def main():
    # connect to s3
    s3_client = boto3.client("s3")
    s3_resource = boto3.resource("s3")
    hadoop_data_bucket = s3_resource.Bucket("unpartitioned")
    #hadoop_files = hadoop_data_bucket.objects.all()
    hadoop_files_2019 = hadoop_data_bucket.objects.filter(Prefix='2019')

    # connect to dynamo db
    dynamo_db = boto3.resource("dynamodb")
    dynamo_table = dynamo_db.Table("sooqr_search_data")
    
    file_counter = 0
    for hadoop_file in hadoop_files_2019:
        hadoop_file_path = "s3://{}/{}".format(hadoop_data_bucket.name, hadoop_file.key)
        
        file_counter+=1
        size = sum(1 for _ in hadoop_data_bucket.objects.filter(Prefix='2019'))
        print("File " + str(file_counter) + " of " + str(size))
        print(hadoop_file_path)

        hadoop_file_df = read_file(hadoop_file_path)
        hadoop_data_grouped = group_data(hadoop_file_df)
        add_record_to_db(dynamo_table, hadoop_data_grouped)
        break
    return True


if __name__ == "__main__":
    main()

    